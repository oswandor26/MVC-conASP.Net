﻿using System.Net;
using System.Reflection.Metadata.Ecma335;
using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;

//importamos el modelo Persona 
using apirest.Models;
using System.Data;

//importamos la libreria Mysqlclient  
using MySql.Data.MySqlClient;
using System.Configuration;

namespace apirest.Controllers;


public class HomeController : Controller
{
    // declaracion de propiedades privas  para el Stringconnection 
    private readonly IConfiguration _configuration; 
    //declaracion List Personas  para almacenar una lista de objetos Personas 
    private static List<Persona> todaslaspersonas = new List<Persona>();

    // declaramos otro ConectrionString para aguardar desde el constructor 
    public String ConectionString; 
    
    // el constructor con el Stringconection de mysql llamos myDb1 de Settings.json 
    public HomeController(IConfiguration configuration){

        _configuration  = configuration;
        // obtenemos el connectionstring de myDb1 en appsetting.json 
        ConectionString = _configuration.GetConnectionString("myDb1");  

    } 


    // index raiz  
    public IActionResult Index()

    {
        // creamos una conecion con ConectString 
        MySqlConnection conneccionbase = new MySqlConnection(ConectionString);  
            
        try
        {
         // abrir la  conecion mysql 
            conneccionbase.Open(); 
    
            Console.WriteLine("Conecction Successful"); 
        // enviamos un comando SQL  retorna objetos           
            MySqlCommand comman = new MySqlCommand("SELECT * FROM CONTACTOS", conneccionbase); 
            MySqlDataReader rd = comman.ExecuteReader(); 
            
            todaslaspersonas = new List<Persona>(); 
            
            while(rd.Read()){
                
                // creamos un objeto del modelo Persona 
                Persona nobjeto = new Persona();             
                nobjeto.id = Convert.ToInt32(rd["ID"]); 
                nobjeto.nombre = rd["nombres"].ToString(); 
                nobjeto.apellido = rd["apellidos"].ToString(); 
                nobjeto.año = rd["age"].ToString(); 
                nobjeto.telefono = rd["telefono"].ToString(); 
                
                // iteramos los usuario de la base de datos y lo almacenamos en un objeto List
                todaslaspersonas.Add(nobjeto);  
                
            }
            
            
            
        // envaimos Lista de Personas a la vista 
        return View(todaslaspersonas);    
        }
        catch (System.Exception)
        {
            
            throw;
        }
    }
    

 [HttpGet] 
 public IActionResult  jsonlist(){  return View();    }    


 //Jsonresult    
public JsonResult getUsers(){

    MySqlConnection conne = new MySqlConnection(ConectionString);  
        
    try
    {
       
        conne.Open(); 
        Console.WriteLine("Connection Successful");
        
        MySqlCommand commandsql = new MySqlCommand("SELECT * FROM CONTACTOS " , conne);  
        
        MySqlDataReader readder = commandsql.ExecuteReader(); 
        
        
        List<Persona>  olistapersonas = new List<Persona>();  
        
        while(readder.Read()){
        
          Console.WriteLine(readder["nombres"] );
          
          
          
          
          Persona nuevapersona = new Persona(); 
          
          nuevapersona.nombre = readder["nombres"].ToString();
          nuevapersona.año = readder["age"].ToString(); 
          nuevapersona.apellido = readder["apellidos"].ToString(); 
          nuevapersona.telefono = readder["telefono"].ToString(); 
          
          
          olistapersonas.Add(nuevapersona); 
          
       
    
        }
        
        Console.WriteLine(olistapersonas.ToString()); 
    
        
        conne.Close(); 
        return Json(olistapersonas);  
        
    }
    catch (System.Exception)
        {
        
        throw;
    }
        
   
} 



// GET: /Home/sendusers vista de formulario para añadir usuarios  
[HttpGet]
public ActionResult sendusers(){
return View(); 
}
    
    
    //POST:  /Home/sendusers?id="" nombre="" apellido="",año="",telefono=""  
    [HttpPost]
public ActionResult sendusers( int id , String nombre , String apellido ,String año , String telefono){
        
            
        MySqlConnection conne = new MySqlConnection(ConectionString);  
            
        try{
        
            conne.Open(); 
            // creamos un objeto MySqlCommand para ejecutar SQL 
            MySqlCommand cmd = new MySqlCommand(); 
            // accedemos al atributo Connection  el cual se pasa la conexion de la base de datos objeto MysqlConnection 
            cmd.Connection = conne;  
           // ejecutamos el comando para insertar usuario pasado de la vista los argumentos 
            cmd.CommandText = String.Format("INSERT INTO CONTACTOS (nombres , apellidos ,age ,telefono  ) VALUES('{0}' , ' {1}' , '{2}' , '{3}')",nombre , apellido , año , telefono); 
            cmd.ExecuteNonQuery();  
            
            conne.Close(); 
        }
        catch (System.Exception ex )
        {
           
           return Content("Ocurrio un Error " + ex.Message); 
        }
        // todo finalizado con exito redirecciona a Index controller      
        return RedirectToAction("Index" , "Home");   
    }



  //GET: editar usuario /Editar?idusers=1   del hipervinculo Editar en Index 
  [HttpGet]
    public ActionResult Editar(int? idusers ){

        if(idusers == null){
            return RedirectToAction("Index" , "Home");
        } 
        Persona? ousuario = todaslaspersonas.Where(c => c.id == idusers ).FirstOrDefault(); 

        return View(ousuario);   
    } 
    


 // editar  usaruio  /Editar?ActuUsers{id , nombre , apellido , año , telefono}  
    [HttpPost]
    public ActionResult Editar(Persona actuUsers){

        MySqlConnection conn  = new MySqlConnection(ConectionString);   

         try
         {  
            conn.Open();

            MySqlCommand comandoact = new MySqlCommand() ;

             comandoact.Connection = conn; 
             comandoact.CommandText = String.Format("UPDATE CONTACTOS SET nombres='{1}'  , apellidos='{2}' ,age='{3}' , telefono='{4}'  WHERE ID={0}" , actuUsers.id  , actuUsers.nombre , actuUsers.apellido , actuUsers.año , actuUsers.telefono);  
             comandoact.ExecuteNonQuery() ;  



             conn.Close(); 

             return RedirectToAction("Index" , "Home") ;
         }
         catch (System.Exception)
         {
             
             throw;
         }
    } 



    public ActionResult Eliminar(int? idusers){
        
        if(idusers == null){

            return RedirectToAction("Index", "Home"); 
        }

        MySqlConnection conn  = new MySqlConnection(ConectionString); 
        try
        {   
            conn.Open(); 
            MySqlCommand comand  = new MySqlCommand();  

            comand.Connection = conn; 
            comand.CommandText = String.Format("DELETE FROM CONTACTOS WHERE ID ={0}" , idusers.ToString()); 
            comand.ExecuteNonQuery(); 

            conn.Close(); 

            return RedirectToAction("Index" , "Home"); 
        }
        catch (System.Exception)
        {
            
            throw;
        }

    }
    
   // public JsonResult senddate(){
    
    
   // List<Persona> objectpersonas = new List<Persona>{ 
   // new Persona { nombre="Ronal" , año=22 , direccion= "col san martin" },     
    //new Persona { nombre="Juan" , age=22 , direccion= "col san martin" }, 
   // }; 
    
    
   // return Json(objectpersonas); 
    
    //}
    
    
    


    

}
