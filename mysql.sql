
-- crear una base de datos 
CREATE DATABASE DBCONTACTO; 

-- usar base de datos  
USE DBCONTACTO ;

CREATE TABLE CONTACTOS( 
ID int NOT NULL   AUTO_INCREMENT, 
nombres VARCHAR(100) ,   
apellidos VARCHAR(100) , 
age VARCHAR(100), 
telefono VARCHAR(100),

PRIMARY KEY(ID)
); 

-- elimiNar tablas  

DROP TABLE NOMBREDELATABLA; 

-- Insertar registro  

INSERT INTO  CONTACTOS ( nombres , apellidos , age , telefono  ) VALUES
    ("Ronal Oswaldo" , "Gonzalez Guardado" , "22" , "71004041"),
    ("Juan Oswaldo" , "Gonzalez felipe" , "22" , "71004041"),
    ("Oscar Oswaldo" , "Gonzalez hernandez" , "22" , "71004041"),
    ("Kevin Oswaldo" , "Gonzalez Guido" , "22" , "71004041"),
    ("Raul Oswaldo" , "Gonzalez Sotos" , "22" , "71004041"),
    ("Douglas Oswaldo" , "Gonzalez Guardado" , "22" , "71004041"); 


-- INSERTAR VALOR  

INSERT INTO CONTACTOS (nombres , apellidos ,age ,telefono  ) VALUES(
     "Evely Marilu" , "Gonzalez Guardado" , "27" , "61004041"
);

-- actulizar  

UPDATE CONTACTOS 
SET apellidos = "Hernezto Morales" 
WHERE ID  = 1 ; 


-- eliminar datos de la tabla 

DELETE FROM CONTACTOS WHERE ID = 2 ;

-- Selecionar 

SELECT * FROM CONTACTOS ; 


--Stored Procedure 
DELIMITER //

Create procedure GetAllProducts()
BEGIN
	SELECT *  FROM CONTACTOS;
END // 

DELIMITER ;



mysql> DELIMITER //
mysql> Create procedure myProcedure (
   IN VACHAR(100) nombre , 
   IN VACHAR(100) apellido ,
   IN VACHAR(100)  ano , 
   IN VACHAR(100) telef
      BEGIN
      
INSERT INTO  CONTACTOS ( nombres , apellidos , age , telefono  ) VALUES(nombre , apellido , ano  , telef);

      END //
Query OK, 0 rows affected (0.20 sec)
mysql> DELIMITER ;






CALL myProcedure ('Raman', 35000, 'Bangalore');
Query OK, 1 row affected (0.45 sec)