namespace apirest.Models;

// creamos un modelo igual que la base de datos 
public class Persona{
    
    public int id {set;get;}
    public String? nombre {set; get;}
    public String? año {set; get;}
    public String? apellido {set; get;}
    
    public String? telefono {set;get;} 

} 
